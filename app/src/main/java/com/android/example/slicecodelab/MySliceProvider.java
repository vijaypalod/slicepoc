package com.android.example.slicecodelab;

import android.net.Uri;

import androidx.annotation.Nullable;
import androidx.slice.Slice;
import androidx.slice.SliceProvider;
import androidx.slice.builders.ListBuilder;

public class MySliceProvider extends SliceProvider {
    @Override
    public boolean onCreateSliceProvider() {
        return true;
    }

    public Slice onBindSlice(Uri sliceUri) {
        switch(sliceUri.getPath()) {
            case "/temperature":
                return createTemperatureSlice(sliceUri);
        }
        return null;
    }

    private Slice createTemperatureSlice(Uri sliceUri) {
        // Construct our parent builder
        ListBuilder listBuilder = new ListBuilder(getContext(), sliceUri, ListBuilder.INFINITY);

        // Construct the builder for the row
        ListBuilder.RowBuilder temperatureRow = new ListBuilder.RowBuilder(listBuilder);

        // Set title
        temperatureRow.setTitle(MainActivity.getTemperatureString(getContext()));

        // TODO: add actions to row; in later step

        // Add the row to the parent builder
        listBuilder.addRow(temperatureRow);

        // Build the slice
        return listBuilder.build();
    }
}